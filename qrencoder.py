import qrcode


class QREncoder:

    def encode(text, save_path='qrCode.jpg'):
        qrImg = qrcode.make(text)
        qrImg.save(save_path)
        return save_path
