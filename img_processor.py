import os
import pickle
import face_recognition
from PIL import Image, ImageFilter


class ImageProcessor():

    def make_blur(self, imgPath, savePath=None):
        if savePath is None: savePath = imgPath
        img = Image.open(imgPath)
        img = img.filter(ImageFilter.BoxBlur(15))
        img.save(savePath)
        img.close()
        return savePath


class FaceDetector:

    def __init__(self):
        if os.path.isfile('memory.pkl'):
            with open('memory.pkl', 'rb') as input:
                self._face_memory = pickle.load(input)
        else:
            self._face_memory = _FaceMemory()

    def detect(self, img_path):
        faces = self._find_faces(img_path)
        result = dict()
        for face in faces:
            name = self._face_memory.check_in_memory(face)
            print(name)
            if name not in result:
                result[name] = list()
            result[name].append(face)
        return result

    def add_face(self, name, faces):
        faces = self._find_faces(faces)
        if len(faces) == 1:
            self._face_memory.add_face(name, faces[0])
        else:
            raise AddingFaceException("Не удалось обнаружить конкретное лицо.")

    def save_memory(self):
        with open('memory.pkl', 'wb') as output:
            pickle.dump(self._face_memory, output, pickle.HIGHEST_PROTOCOL)

    def _find_faces(self, img_path):
        img = face_recognition.load_image_file(img_path)
        img_pil = Image.open(img_path)
        face_locations = face_recognition.face_locations(img)
        n = 0
        faces = []
        for face_loc in face_locations:
            crop_img = img_pil.crop(self._transform_loc(face_loc))
            crop_img_name = self._name_of_part(img_path, n)
            crop_img.save(crop_img_name)
            faces.append(crop_img_name)
            n+=1
        return faces

    @staticmethod
    def _name_of_part(img_name, part: int):
        splited_name = img_name.split('.')
        return "{}_{}.{}".format(splited_name[0], str(part), splited_name[1])

    @staticmethod
    def _transform_loc(loc):
        """(top, right, bottom, left) to (left, upper, right, lower)"""
        return loc[3], loc[0], loc[1], loc[2]


class _FaceMemory:

    def __init__(self):
        self._memory = dict()

    def check_in_memory(self, face):
        face_img = face_recognition.load_image_file(face)
        face_encoding = face_recognition.face_encodings(face_img)
        if len(face_encoding) > 0:
            for key in self._memory:
                val = self._memory[key]
                results = face_recognition.compare_faces(val, face_encoding[0])
                if self._is_detected(results):
                    return key
            return "Информации нет"
        return "Лицо плохо различимо"

    @staticmethod
    def _is_detected(results):
        for result in results:
            if result:
                return True
        return False

    def add_face(self, name, face):
        face_img = face_recognition.load_image_file(face)
        face_encoding = face_recognition.face_encodings(face_img)
        if len(face_encoding) > 0:
            if name not in self._memory:
                self._memory[name] = list()
            self._memory[name].append(face_encoding[0])
        else:
            raise AddingFaceException("Лицо плохо различимо")


class AddingFaceException(Exception):

    def __init__(self, message):
        self.message = message