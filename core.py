import random
from enum import Enum, auto

import requests
import vk_api
from vk_api.keyboard import VkKeyboard
from vk_api.longpoll import VkLongPoll, VkEventType


class MessageTypes(Enum):
    MESSAGE_TO_ME = auto()
    MESSAGE_FROM_ME = auto()



class VkBot:

    def __init__(self, token):
        self.vk_session = vk_api.VkApi(token=token)
        self._uploader = vk_api.VkUpload(self.vk_session)
        self.longpoll = VkLongPoll(self.vk_session, wait=600)
        self.vk = self.vk_session.get_api()
        self._handlers = dict();

    def _upload_photos(self, photos):
        uploaded_photos = self._uploader.photo_messages(photos)
        attachments = []
        for photo in uploaded_photos:
            attachments.append('photo{}_{}'.format(photo['owner_id'], photo['id']))
        return attachments

    def _upload_documents(self, docs, peer_id):
        uploaded_photos = []
        for doc in docs:
            uploaded_photos.append(self._uploader.document_message(docs, doc, doc, peer_id))
        attachments = []
        for photo in uploaded_photos:
            attachments.append('doc{}_{}'.format(photo['owner_id'], photo['id']))
        return attachments

    def _handel(self, type: MessageTypes, event):
        if type in self._handlers:
            for handler in self._handlers[type]:
                handler(event)

    def _parametrized(dec):
        def layer(self, *args, **kwargs):
            def repl(f):
                return dec(self, f, *args, **kwargs)

            return repl

        return layer

    def start(self):
        for event in self.longpoll.listen():
            try:
                if event.type == VkEventType.MESSAGE_NEW:
                    if event.to_me:
                        self._handel(MessageTypes.MESSAGE_TO_ME, event)
                    elif event.from_me:
                        self._handel(MessageTypes.MESSAGE_FROM_ME, event)
                else:
                    pass
            except Exception as e:
                raise e

    def send_message(self, user_id=None, message=' ', photos=[], documents=[], keyboard=None):
        attachments = []
        if len(photos) > 0:
            attachments.extend(self._upload_photos(photos))
        if len(documents) > 0:
            attachments.extend(self._upload_documents(documents, user_id))
        self.vk.messages.send(
            user_id=user_id,
            attachment=','.join(attachments),
            message=message,
            keyboard=keyboard.get_keyboard() if keyboard is not None else None,
            random_id=random.randint(2147483647, 9223372036854775807)
        )

    @_parametrized
    def handler(self, fn, message_type: MessageTypes):
        if message_type not in self._handlers:
            self._handlers[message_type] = list()
        self._handlers[message_type].append(fn)
        return fn


class DownloadManager:

    def __init__(self, vk_bot):
        self._vk_bot = vk_bot

    def _get_attach(self, attach_type=None):
        if attach_type:
            ls = list()
            for key in self._attachments:
                if self._attachments[key] == attach_type:
                    ls.append(key)
            return ls
        else:
            return self._attachments

    def download_photo(self, peer_id):
        return self._download(peer_id, 'photo', 'jpg')

    def download_audio(self, peer_id):
        return self._download(peer_id, 'audio', 'mp3')

    def _download(self, peer_id, attach_type, file_extension):
        to_download = self._get_urls(peer_id, attach_type)
        print(to_download)
        files = []
        for file in to_download:
            file_name = "{}.{}".format(file, file_extension)
            with open(file_name, 'wb') as handle:
                response = requests.get(to_download[file], stream=True)
                if not response.ok:
                    print(response)
                for block in response.iter_content(1024):
                    if not block:
                        break
                    handle.write(block)
            files.append(file_name)
        return files

    def _get_img_size(self, sizes, size_type):
        for size in sizes:
            if size['type'] == size_type:
                return size

    def _parse(self, attachments, attach_type):
        urls = dict()
        for attach in attachments:
            print(attachments)
            if attach['type'] == attach_type:
                if attach_type == 'photo':
                    photo_id = attach['photo']['id']
                    size = None
                    for size_type in ['w', 'y', 'z', 'x', 'm', 's']:
                        size = self._get_img_size(attach['photo']['sizes'], size_type)
                        if size is not None:
                            break
                    url = size['url']
                    urls[photo_id] = url
                elif attach_type == 'audio':
                    audio_id = attach['audio']['id']
                    url = attach['audio']['url']
                    urls[audio_id] = url
        return urls

    def _get_urls(self, peer_id, attach_type):
        attachments = self._vk_bot.vk.messages.getHistory(user_id=peer_id, count=1)['items'][0]['attachments']
        return self._parse(attachments, attach_type)


class Button():
    """
        Class Button.
        For initialize button with params.
        The constructor takes a text of button, color, wrether to add line and special param for hide text in list.
    """

    def __init__(self, text, color=Colors.BLUE, newLine=False, hideTxt=False):
        self.text = text
        self.color = color
        self.newLine = newLine
        self.hideTxt = hideTxt


class Keyboard():
    """
        Class Keyboard.
        For initialize VK Keyboard.
        The constructor takes a tuple of buttons.
    """

    def __init__(self, *button):
        if not type(button) is tuple:
            self.buttons = (button,)
        else:
            self.buttons = button

    def getActionId(self, id=None, text=None):
        """
            Return id of button in list by number or text.
            If buttons is not exist it returns None.
        """
        for index, i in enumerate(self.buttons, start=1):
            if id == index or text == i.text:
                return index
        return None

    def getList(self, startLine=''):
        """
            Return list of avaliable buttons by text.
        """
        butlist = ''
        for index, i in enumerate(self.buttons, start=1):
            if not i.hideTxt:
                butlist += startLine + str(index) + ') ' + i.text + '\n'
        return butlist

    def getKeyboard(self):
        """
            Return list of avaliable buttons by VK Keyboard.
        """
        keyboard = VkKeyboard(one_time=True)
        for i in self.buttons:
            keyboard.add_button(i.text, i.color.value)
            if i.newLine: keyboard.add_line()
        return keyboard.get_keyboard()
