from time import sleep

from vk_api.longpoll import Event

from core import VkBot, MessageTypes, DownloadManager
from img_processor import FaceDetector, AddingFaceException

vk_bot = VkBot("c96bdf55402abc714535a0c842ecbb4510df84171518d37c25d64ddd17da63137e5752db0724e14337f0e")
download_manager = DownloadManager(vk_bot)
face_detector = FaceDetector()


@vk_bot.handler(MessageTypes.MESSAGE_TO_ME)
def new_message(event: Event):
    if event.text:
        try:
            photo = download_manager.download_photo(event.peer_id)
            if len(photo) == 1:
                face_detector.add_face(event.text, photo[0])
                face_detector.save_memory()
                vk_bot.send_message(event.user_id, "Я запомнил этого человека")
            else:
                vk_bot.send_message(event.user_id, "Пришли одну фотографию")
        except AddingFaceException as e:
            vk_bot.send_message(event.user_id, e.message)
    else:
        for photo in download_manager.download_photo(event.peer_id):
            faces = face_detector.detect(photo)
            if len(faces) == 0:
                vk_bot.send_message(event.user_id, "Я никого не нашел")
            else:
                for face in faces:
                    vk_bot.send_message(event.user_id, face, faces[face])


def start():
    while True:
        try:
            vk_bot.start()
        except:
            print("Restart BOT")
            sleep(30)


if __name__ == '__main__':
    start()
