class _Сrypto:

    def encrypt(self, text):
        new_text = ""
        for i in text:
            if i.isalpha():
                new_text += self.shiftAlpha(i, self._get_key())
            else:
                new_text += i
        return new_text

    def decrypt(self, text):
        new_text = ""
        for i in text:
            if i.isalpha():
                new_text += self.shiftAlpha(i, -self._get_key())
            else:
                new_text += i
        return new_text

    def _get_key(self):
        raise NotImplementedError("Please Implement this method")


    @staticmethod
    def _shift_cycle(a, b, n, count):
        n += count
        if n > b:
            n -= b - a + 1
        elif n < a:
            n += b - a + 1
        return n

    def _shift_alpha(self, letter, count):
        let = ord(letter)
        if ord('A') <= let <= ord('Z'):
            let = self.shift_cycle(ord('A'), ord('Z'), let, count)
        elif ord('А') <= let <= ord('Я'):
            let = self.shift_cycle(ord('А'), ord('Я'), let, count)
        elif ord('a') <= let <= ord('z'):
            let = self.shift_cycle(ord('a'), ord('z'), let, count)
        elif ord('а') <= let <= ord('я'):
            let = self.shift_cycle(ord('а'), ord('я'), let, count)
        letter = chr(let)
        return letter


class Caesar(_Сrypto):

    def _get_key(self):
        return 3


class Viginer(_Сrypto):

    def __init__(self, key):
        self._key = key
        self._text_index = 0

    @staticmethod
    def _index_alpha(letter):
        let = ord(letter)
        if ord('A') <= let <= ord('Z'):
            let -= ord('A')
        elif ord('А') <= let <= ord('Я'):
            let -= ord('А')
        elif ord('a') <= let <= ord('z'):
            let -= ord('a')
        elif ord('а') <= let <= ord('я'):
            let -= ord('а')
        else:
            let = -1
        return let + 1

    def _get_key(self):
        key = self.indexAlpha(self.key[self._text_index % len(self._key)])
        self._text_index += 1
        return key
